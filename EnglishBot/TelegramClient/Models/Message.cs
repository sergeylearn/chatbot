﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.TelegramClient.Models
{
    class Message
    {
        [JsonProperty("message_id")] // Serialize the member MessageId to "message_id"
        public int MessageId { get; set; }
        public int Date { get; set; }

    }
}
