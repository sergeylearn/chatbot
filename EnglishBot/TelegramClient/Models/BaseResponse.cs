﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.TelegramClient.Models
{
    class BaseResponse<TResult>
    {        
        public bool Ok { get; set; }
        [JsonProperty("error_code")]
        public int ErrorCode { get; set; }
        public string Description { get; set; }
        public TResult Result { get; set; }
    }
}
