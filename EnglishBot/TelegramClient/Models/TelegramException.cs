﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.TelegramClient.Models
{
    class TelegramException:Exception
    {
        public int ErrorCode { get; }
        public string Description { get; }

        public TelegramException(int errorCode, string description) : base("Error in Telegram: " + description)
        {
            ErrorCode = errorCode;
            Description = description;           
        }
    }
}
