﻿using EnglishBot.TelegramClient.Models;
using EnglishBot.VkClient.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.TelegramClient
{
    class TelegramClient
    {
        private readonly string _token;
        private readonly string _baseUrl;

        public TelegramClient(string token, string baseUrl)
        {
            _token = token;
            _baseUrl = baseUrl;
        }

        /// <summary>
        /// Отправка сообщения в Телеграм
        /// </summary>
        /// <param name="chatId">id чата Телеграма, в которое отправляем сообщение</param>
        /// <param name="text">текст сообщения</param>
        /// <returns></returns>
        public Message SendMessage(string chatId, string text)
        {
            var url = $"{_baseUrl}{_token}/sendMessage";
            var request = WebRequest.CreateHttp(url);
            request.ContentType = "application/json";
            request.Method = "POST";
            try
            {
                var requestJson = JsonConvert.SerializeObject(new
                {
                    chat_id = chatId,
                    text
                });
                // Получаем объект Stream для записи туда тела запроса
                // Stream -- это объект потока, (аналогия -- конвейер)
                var requestStream = request.GetRequestStream();  
                // StreamWriter -- утилита, оборачивающая поток и складирующая туда данные
                var streamWriter = new StreamWriter(requestStream);
                // Через StreamWriter записываем в поток подготовленную строку в формате json
                streamWriter.WriteLine(requestJson);
                // Сбрасываем все, что подготовили для записи, в поток
                streamWriter.Flush();
                // Получаем ответ
                var response = (HttpWebResponse) request.GetResponse();
                // Получаем поток ответа и создаем обертку для его чтения
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    // Читаем весь поток ответа до конца потока
                    var responseString = streamReader.ReadToEnd();
                    // Десериализируем тело ответа из JSON
                    var responseTelegram = JsonConvert.DeserializeObject<BaseResponse<Message>>(responseString);
                    if (responseTelegram.Ok== false)
                    {
                        throw new TelegramException(responseTelegram.ErrorCode, responseTelegram.Description);
                    }
                    return responseTelegram.Result;
                   
                }

             
            }
            // Обрабатываем случай, когда не приходит успешный ответ
            catch (WebException webException)
            {
                if (webException.Status == WebExceptionStatus.ProtocolError)
                {
                    // Получаем поток ответа и создаем обертку для его чтения
                    using (var streamReader = new StreamReader(webException.Response.GetResponseStream()))
                    {
                        // Читаем весь поток ответа до конца потока
                        var responseString = streamReader.ReadToEnd();
                        var responseTelegram = JsonConvert.DeserializeObject<BaseResponse<Message>>(responseString); // Десериализируем тело ответа из JSON
                        if (responseTelegram.Ok == false)
                        {
                            throw new TelegramException(responseTelegram.ErrorCode, responseTelegram.Description);
                        }
                        return responseTelegram.Result;                       
                       
                    }
                }
                else
                {
                    throw;
                }
                

            }
        }
        public Message SendPhoto(string chatId, string photo)
        {
            var url = $"{_baseUrl}{_token}/sendPhoto";
            var request = WebRequest.CreateHttp(url);
            request.ContentType = "application/json";
            request.Method = "POST";
            try
            {
                var requestJson = JsonConvert.SerializeObject(new
                {
                    chat_id = chatId,
                    photo
                });
                // Получаем объект Stream для записи туда тела запроса
                // Stream -- это объект потока, (аналогия -- конвейер)
                var requestStream = request.GetRequestStream();
                // StreamWriter -- утилита, оборачивающая поток и складирующая туда данные
                var streamWriter = new StreamWriter(requestStream);
                // Через StreamWriter записываем в поток подготовленную строку в формате json
                streamWriter.WriteLine(requestJson);
                // Сбрасываем все, что подготовили для записи, в поток
                streamWriter.Flush();
                // Получаем ответ
                var response = (HttpWebResponse)request.GetResponse();
                // Получаем поток ответа и создаем обертку для его чтения
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    // Читаем весь поток ответа до конца потока
                    var responseString = streamReader.ReadToEnd();
                    // Десериализируем тело ответа из JSON
                    var responseTelegram = JsonConvert.DeserializeObject<BaseResponse<Message>>(responseString);
                    if (responseTelegram.Ok == false)
                    {
                        throw new TelegramException(responseTelegram.ErrorCode, responseTelegram.Description);
                    }
                    return responseTelegram.Result;

                }


            }
            // Обрабатываем случай, когда не приходит успешный ответ
            catch (WebException webException)
            {
                if (webException.Status == WebExceptionStatus.ProtocolError)
                {
                    // Получаем поток ответа и создаем обертку для его чтения
                    using (var streamReader = new StreamReader(webException.Response.GetResponseStream()))
                    {
                        // Читаем весь поток ответа до конца потока
                        var responseString = streamReader.ReadToEnd();
                        var responseTelegram = JsonConvert.DeserializeObject<BaseResponse<Message>>(responseString); // Десериализируем тело ответа из JSON
                        if (responseTelegram.Ok == false)
                        {
                            throw new TelegramException(responseTelegram.ErrorCode, responseTelegram.Description);
                        }
                        return responseTelegram.Result;

                    }
                }
                else
                {
                    throw;
                }


            }
        }

    }
    
}
