﻿using EnglishBot.VkClient.Infrastructure;
using EnglishBot.VkClient.Models;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net;

namespace EnglishBot.VkClient
{
    class VkClient
    {
        private readonly string _baseUrl;
        private readonly string _accessToken;
        private readonly string _version;
        private readonly WebClient _webClient;

        public VkClient(string accessToken, string version)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new ArgumentNullException(nameof(accessToken));
            }
            _baseUrl = ConfigurationManager.AppSettings["vkBaseUrl"];
            _accessToken = accessToken;
            _version = version;
            _webClient = new WebClient();
        }

        /// <summary>
        /// Получение записей со стены
        /// </summary>
        /// <returns>Записи со стены</returns>
        public ResponseList<Post> GetWall(string domain, int count, string filter)
        {
            var url =
                $"{_baseUrl}method/wall.get?v={_version}&access_token={_accessToken}&domain={domain}&count={count}&filter={filter}";
            var result = _webClient.DownloadString(url);
            var response = JsonConvert.DeserializeObject<BaseResponse<ResponseList<Post>>>(result, new AttachmentConverter());
            return response.Error != null
                ? throw new VkException(response.Error.ErrorCode, response.Error.ErrorMessage)
                : response.Response;
        }
    }
}
