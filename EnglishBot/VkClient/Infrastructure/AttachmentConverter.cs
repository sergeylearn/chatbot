﻿using EnglishBot.VkClient.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.VkClient.Infrastructure
{
    class AttachmentConverter : JsonConverter<IAttachment>
    {

        public override IAttachment ReadJson(JsonReader reader, Type objectType, IAttachment existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            JObject jsonObject = JObject.Load(reader);
            string type = (string) jsonObject["type"];
            switch (type)
            {
                case "photo":
                    return jsonObject["photo"].ToObject<Photo>();                    
                default:
                    return new Attachment();
            }
        }

        public override void WriteJson(JsonWriter writer, IAttachment value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

    }
}
