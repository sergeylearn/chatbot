﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.VkClient.Infrastructure
{
    class VkException: Exception
    {
        public int ErrorCode { get; }
        public string ErrorMessage { get; }

        public VkException(int errorCode, string errorMessage): base("Error in VK API: " + errorMessage)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
        }
    }
}
