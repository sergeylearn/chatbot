﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnglishBot.VkClient.Models
{
    class BaseResponse<TResponse>
    {
        public TResponse Response { get; set; }

        public Error Error { get; set; }
    }
}
