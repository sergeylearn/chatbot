﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.VkClient.Models
{
    class Photo : IAttachment
    {
        public int Id { get; set; }

        [JsonProperty("user_id")]
        public int UserId { get; set; }

        public List<PhotoSize> Sizes {get;set;}
    }

    class PhotoSize
    {
        public string Type { get; set; }

        public string Url { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }
    }
}
