﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.VkClient.Models
{
    class Post
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public List<IAttachment> Attachments { get; set; }
        
    }
}
