﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.VkClient.Models
{
    class ResponseList<TItem>
    {
        public int Count { get; set; }

        public List<TItem> Items { get; set; }
    }
}
