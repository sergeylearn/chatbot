﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishBot.VkClient.Models
{
    class Error
    {
        [JsonProperty("error_code")] // Serialize the member ErrorCode to "error_code"
        public int ErrorCode { get; set; }

        [JsonProperty("error_msg")] // Serialize the member ErrorMsg to "error_msg"
        public string ErrorMessage { get; set; }
    }
}
