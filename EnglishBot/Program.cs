﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EnglishBot.VkClient;
using EnglishBot.VkClient.Infrastructure;
using System.Configuration;
using EnglishBot.TelegramClient.Models;
using EnglishBot.VkClient.Models;

namespace EnglishBot
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Requesting data from VK");
                var vkAccessToken = ConfigurationManager.AppSettings["vkAccessToken"];
                var version = ConfigurationManager.AppSettings["version"];
                var vkClient = new VkClient.VkClient(vkAccessToken, version);
                var vkResponse = vkClient.GetWall("english.club.tiraspol", 3, "owner");
                Console.WriteLine("Received data from VK");
                Console.WriteLine("Sending message to Telegram");
                var telegramBaseUrl=ConfigurationManager.AppSettings["telegramBaseUrl"];
                var telegramToken = ConfigurationManager.AppSettings["telegramToken"];
                var telegramClient = new TelegramClient.TelegramClient(telegramToken, telegramBaseUrl);
                telegramClient.SendMessage("-1001435730192", vkResponse.Items[0].Text);
                telegramClient.SendPhoto("-1001435730192", ((Photo)vkResponse.Items[0].Attachments[0]).Sizes[0].Url);
                Console.WriteLine("Message sent");
            }
            catch (VkException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch (TelegramException exception)
            {
                Console.WriteLine(exception.Message);
            }
            Console.ReadKey();


        }
    }
}
